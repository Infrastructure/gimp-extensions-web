# Infrastructure for GIMP extensions

This document contains some basic information about the whole extension
system of GIMP. It is likely to evolve a lot as we are still working on
it.

## Extension format

A GIMP extension has an **unique** identifier in reverse-DNS scheme. For
instance, say the GIMP core team decided to remove some old brushes from
default GIMP and publish these as an extension (so that anyone would
still be able to install these old brushes, while keeping core GIMP of
highest quality). This extension could be called for instance
`org.gimp.legacy.brushes`. If *ZeMarmot* project wanted to publish some
brushes to paint trees, an acceptable name for the extension could be
`net.zemarmot.film.tree_brushes`.

The extension itself is a compressed file (currently `zip` format, but
this is not set in stone yet), which makes GIMP extensions easy to share
and distribute as single-files, as the new `.gex` format.

It contains at its root a single directory named as the extension ID.
Inside this directory, a metadata file is present, named as the
extension ID appended with `.metainfo.xml`. This file uses the
[AppStream format](https://www.freedesktop.org/software/appstream/docs/).

Finally it could have any random data files. These additional files will
have to be declared inside the metadata in order for GIMP to know about
them and to be able to load them.

For instance our extension `org.gimp.legacy.brushes` could have this
structure:

```
 org.gimp.legacy.brushes.gex:
   org.gimp.legacy.brushes/
     org.gimp.legacy.brushes.metainfo.xml
     brushes/
       somebrush.gbr
       otherbrush.gbr
       funnybrush.gih
       …
```

### AppStream metadata
#### Mandatory fields

A GIMP extension metadata is mandatorily a [component of type
"addon"](https://www.freedesktop.org/software/appstream/docs/sect-Metadata-Addon.html).

Therefore it must have these fields:

* id
* extends: with the value `org.gimp.GIMP`
* name
* summary
* metadata_license

Additionally our repository requires some additional fields because the
goal is for people to understand what they are going to install.
Extensions with just a name and a few words are not very useful. You'd
also want:

* project_license
* description
* screenshots: at least one image
* releases: tagging at least your releases with a version and a date

#### Recommended fields

For completeness, we would recommend to add some `<url/>` tags if
relevant, in particular "homepage" and "bugtracker" so that people are
able to get more information and raise any issues your extension may
have.

On our side, our infrastructure should be able to display the "donation"
url if available to help extension creators.

#### Requirements

If the creator knows that one's extension won't work properly for older
or newer version of GIMP, we would recommend to set the `<requires>`
tag. For instance, say you are developing a plug-in, using new APIs
which appeared in 2.10.2. On the other hand, it uses a deprecated API
which got removed in GIMP 3.0.

The requires tag could be:

```xml
<requires>
  <id version="2.10.2" compare="ge">org.gimp.GIMP</id>
  <id version="3.0" compare="lt">org.gimp.GIMP</id>
</requires>
```

Now another great feature we'd want to support is dependency to another
extension. Say your extension also depends on another. When we install
it, we'd want the system to pull this dependency (of course it would not
be done silently and would require approval):

```xml
<requires>
  <id version="1.0" compare="ge">org.example.other_extension</id>
  <id version="2.10.2" compare="ge">org.gimp.GIMP</id>
  <id version="3.0" compare="lt">org.gimp.GIMP</id>
</requires>
```

Finally a major requirement type would be OS requirements for extensions
which can only be run on specific platforms, or on specific versions of
platforms. For instance, a Linux-only plug-in could be advertized this
way:

```xml
<requires>
  <kernel version="4.14" compare="ge">Linux</kernel>
</requires>
```

This would work the same for Windows, macOS or various BSD kernels. We'd
just have to come up with the right standard to name and number them
uniquely (using the internal release of API versionning not the marketing
names of commercial OSes in particular).

#### Examples
##### A brush extension

A simple set of brushes only, which are stored under the subdirectory
`brushes/`.

```xml
<?xml version="1.0" encoding="UTF-8"?>
<component type="addon">
  <id>org.example.nature_brushes</id>
  <extends>org.gimp.GIMP</extends>

  <name>Nature brush set</name>
  ⁠<developer_name>Awesome team</developer_name>
  <summary>A collection of brushes to draw natural elements</summary>
  <description>
    <p>This collection contains many brushes specifically to draw
    natural elements:</p>
    <ul>
      <li>Leaves</li>
      <li>Clouds</li>
      <li>Dirt</li>
    </ul>
  </description>

  <url type="homepage">https://example.org/nature_brushes</url>
  <url type="donation">https://example.org/donate</url>

  <metadata_license>CC0-1.0</metadata_license>
  <project_license>CC0-1.0</project_license>

  <releases>
    <release version="0.2" date="2019-02-19" />
    <release version="0.1" date="2018-06-07" />
  </releases>
  <metadata>
    <value key="GIMP::brush-path">brushes</value>
  </metadata>
</component>
```

##### A plug-in

This plug-in requires the brush extension (version 0.2 or higher) to be
installed too so it adds a requirement. Also it requires GIMP 2.10.6 or
higher (maybe because it uses some new API), and lesser than GIMP 3.0
(API change, the plug-in won't work).

This will allow to pull an extension dependency (i.e. another extension)
after warning the user it will do so, and also warn/disable extensions
when we know an extension won't work (an application can then propose to
check if updates of the extensions are available).

```xml
<?xml version="1.0" encoding="UTF-8"?>
<component type="addon">
  <id>org.example.tree_generator</id>
  <extends>org.gimp.GIMP</extends>

  <name>Generator of trees</name>
  <summary>Plug-in randomly generating trees</summary>
  <description>
    <p>This plug-in will randomly generate a forest inside the
    selection.</p>
  </description>

  <url type="homepage">https://example.org/tree_generator</url>
  <url type="donation">https://example.org/donate</url>

  <metadata_license>CC0-1.0</metadata_license>
  <project_license>GPL-3.0+</project_license>

  <releases>
    <release version="0.1" date="2019-02-20" />
  </releases>
  <requires>
    <!-- Uses an API appeared in 2.10.6. -->
    <id version="2.10.6" compare="ge">org.gimp.GIMP</id>
    <id version="3.0.0" compare="lt">org.gimp.GIMP</id>
    <id version="0.2" compare="ge">org.example.nature_brushes</id>
  </requires>
  <metadata>
    <value key="GIMP::plug-in-path">src/tree-generator.py</value>
  </metadata>
</component>
```

#### Library Software

One of the nice advantage of AppStream is that it is an extensively used
standard to describe software, which has been widely used by
distributions to display and search for software to install (e.g. GNOME
Software or KDE Discover). So it's nice and widespread. It also means
that there are already a few libs reading such files.

In GIMP, we use [appstream-glib](https://github.com/hughsie/appstream-glib)
(used to not work on Windows, but I [Jehan] patched it so that it now
works fine on all major platforms supported by GIMP). Note that all GIMP
2.99.x development versions are already bundled with some support of the
`.gex` format and use this lib, which is a good trial by fire.

There is also a more generic [appstream](https://github.com/ximion/appstream)
library which has several API (including another GObject one and a Qt5
one). I don't know its current multi-platform support (back in 2018, it
used to also not build/work on Windows, and it turns out I also have
patches to make it work, but I forgot to contribute them as I got
diverted into appstream-glib instead; not sure if they still apply or
are even needed nowadays).

## Support in GIMP
### Repositories

A default repository maintained by the GIMP core team will be set by
default. There is probably no reason to forbid third-party repositories
so these could be supported (added through GIMP Preferences).

GIMP will simply download a static file from an extension repository,
which will contain the concatenated metadata for all available
extensions.

Of course some layers of security would be needed (certificates,
checksums, signed packages…).

The repository format is also based on AppStream, in particular the
"Collection Metadata" part of the format:
https://www.freedesktop.org/software/appstream/docs/chap-CollectionData.html

The extension download path can be generated from the extension ID and
the version as advertized in the collection.

### Why a single file?

There is all the talk about having an "API" so software could "query"
the extension server, make searches and whatnot. It's all very fancy and
much more "fashionable" than old-style repository-data-in-one-file
scheme. Why I went with the simple and single file download logics is:

* Allows for extension browsing offline: maybe because the person has
  slow or limited connection, on the move without ability to connect, or
  any other reason (or even when the extension server itself is
  temporarily down), you might want to browse the available extensions
  (even if a bit outdated) without constantly querying the server.
* Simpler server code: while it pushes the logics of searching for
  extensions on the client side, it also allows much simpler service
  side logics. And in particular we can even have a mostly static
  server, which means easier maintenance and safer (see below the
  section about `Static Code` on the server when possible).
* Much faster and distributed search logics: instead of always asking
  the server to do searches for you through a web API and wait for a
  response, all the data is already present on your hard disk as a
  single file. Searching through it should be pretty instant.

This "repository info as a single file" logics has also been tried and
proved for decades as this is how work most repository systems for
GNU/Linux distributions (RPM, Deb, etc.).

### TODO: signing

There is obviously an aspect which will need to be taken care of: the
repository files should be signed, as well as individual packages to
avoid various types of data tampering (man-in-the-middle or other).

This has not been specified yet, but should likely be done similarly to
how various GNU/Linux distributions have been doing.

### Updating the repository data

Depending on how software download the repository file, it might not be
the best idea to simply switch the XML file with a new file. If we look
at a RPM repository for instance, the main entry point is always a very
short `repomd.xml` which simply lists the current file with detailed
package data.

This allows to not just get rid suddenly of a slightly outdated version
of the repository snapshot, especially while someone is looking at it.
So the process would be to have our own equivalent of `repomd.xml`,
which contains the name of the current repository file and its checksum.

A sane process to update the repository file would be:

* Add the updated repository listing named for instance
  `<some-sha-checksum>-extensions.xml.xz`
* Update the `repomd.xml` (or equivalent file) to point to the new
  listing.
* Finally remove the old listing (note that this third step can even be
  done a bit later, allowing for some types of partial downloads in
  steps to not fail).

#### Example

Our repository containing only the 2 extensions above will serve the
following file:

```xml
<?xml version="1.0" encoding="UTF-8"?>
<components version="0.12">
  <component type="addon">
    <id>org.example.nature_brushes</id>
    <extends>org.gimp.GIMP</extends>

    <name>Nature brush set</name>
    <developer_name>Awesome team</developer_name>
    <summary>A collection of brushes to draw natural elements</summary>
    <description>
      <p>This collection contains many brushes specifically to draw
      natural elements:</p>
      <ul>
        <li>Leaves</li>
        <li>clouds</li>
        <li>dirt</li>
      </ul>
    </description>

    <url type="homepage">https://example.org/nature_brushes</url>
    <url type="donation">https://example.org/donate</url>

    <metadata_license>CC0-1.0</metadata_license>
    <project_license>CC0-1.0</project_license>

    <releases>
      <release version="0.2" date="2019-02-19" />
      <release version="0.1" date="2018-06-07" />
    </releases>
    <metadata>
      <value key="GIMP::brush-path">brushes</value>
    </metadata>
  </component>

  <component type="addon">
    <id>org.example.tree_generator</id>
    <extends>org.gimp.GIMP</extends>

    <name>Generator of trees</name>
    <summary>Plug-in randomly generating trees</summary>
    <description>
      <p>This plug-in will randomly generate a forest inside the
      selection.</p>
    </description>

    <url type="homepage">https://example.org/tree_generator</url>
    <url type="donation">https://example.org/donate</url>

    <metadata_license>CC0-1.0</metadata_license>
    <project_license>GPL-3.0+</project_license>

    <releases>
      <release version="0.1" date="2019-02-20" />
    </releases>
    <requires>
      <!-- Uses an API appeared in 2.10.6. -->
      <id version="2.10.6" compare="ge">org.gimp.GIMP</id>
      <id version="3.0.0" compare="lt">org.gimp.GIMP</id>
      <id version="0.2" compare="ge">org.example.nature_brushes</id>
    </requires>
    <metadata>
      <value key="GIMP::plug-in-path">src/tree-generator.py</value>
    </metadata>
  </component>
</components>
```

### Dedicated GUI

The GUI would allow searching (through extension names, descriptions,
etc.), viewing extension details, installing (by downloading the actual
file), uninstalling, updating extensions, etc.

### Notifications

If an already installed extension is available in a newer version, GIMP
should notify the user and propose to update even when not looking at
the dedicated GUI.

## Web infrastructure
### Static Code (when possible)

Something we learned with years of doing Free Software is that
contributor time is not infinite, we cannot (and don't want to) ask
people to fix things on a timely manner a Sunday evening, and so on. So
we must be extra-careful and have simpler web code. This is even truer
as making website is not our thing. We do a desktop software and want to
have to touch the website as rarely as possible.
And one of the best way to be careful and safe is to have mostly static
code, which just allows nearly no attack surface (code injection or the
like). This is why gimp.org and many other Free Software websites are
static.

Now here it might seem weird because for the extension website, we need
creators to be able to upload their new extension update, then to
generate new pages from these. But I am thinking this can be done in a
slightly asynchronous way then we could have fully static pages for
extensions, with only a minimum of dynamic code (extension upload only
possibly).

As for comment or notation, if we ever want this (we might), Pat David
from pixls.us told us we could make use of their infrastructure.
Therefore we may be able to have mostly static pages which comments
handled by pixls.us infra.

### User point-of-view

Though extensions can be searched within GIMP itself, it should also be
possible to view the list of extensions in a web browser, and detailed
extension pages, without accounts.

Note that it should be possible also to comment (and maybe note?)
extensions. These possibilities may require accounts though.

Finally anyone should be able to flag any extension deemed dangerous
(whether on purpose or because of unexpected issues), with some
description explaining the issue.

### Creator point-of-view

An extension creator would need an account to be able to create a new
extension page then publish new versions of an extension. Publishing a
new version means simply uploading a `.gex` file following the format
described above. The system would have to make sure that the format
requirements are respected, that the version number got bumped, and so
on.

It should also check that the contents corresponds to the advertized
data.

Finally it should flag potentially dangerous data for review before
actually publishing it. I.e. in particular plug-ins and scripts would
require review before being distributed.

Binary plug-ins should be forbidden except for specific creators well
known and trusted. This last requirement can only be lifted in a future
where our infrastructure will be able to compile these binary plug-ins
from source.

Note: the idea has been floating around to be able to track a source
repository and automatically generate new releases upon tagging. This
may be a possible future path for helping extension maintainership.

### Reviewer point-of-view

This whole system can only work if we gather a trusted community from
which we can promote "reviewers". These reviewers would have the role to
accept or refuse extensions needing reviews, as well as make decisions
on explicitly flagged extensions.

Detailed review rules would have to be decided.

### Curator point-of-view

It may be interesting to be able to put forward specific extensions, for
any reason. It might be interesting to create a "curator" role and have
some people in the community promoted to such role, giving them some
power to recommend extensions to a wide audience (unless we want
automatic curation only, based on number of downloads for instance).

### Security and code review of third-party code

One of the extension type would be obviously plug-ins. This is the most
critical part of the system, especially as GIMP doesn't have yet any
type of sandbox for its plug-ins.

Therefore just being middle-men for random developers of untrusted code
is dangerous, if not irresponsible. On a first (simpler) version, we
should probably:

* Only allow non-compiled plug-ins, whose code can therefore be
  inspected.
* Not make plug-in extensions automatically available to people (unlike
  data-type extensions when they are deemed safe). They have to go at
  least through a first set of review by community reviewers.
  Optionnally we may make the plug-ins available before review, but with
  clear warning "**This has not been reviewed yet**".
* Even after a reviewer's validation, the platform needs a way for
  people to easily check the code and to warn us if they spot a flaw
  (malevolent or by mistake) in an extension which makes it potentially
  dangerous.
* Some specific people or team well known to the community, with their
  own well-trusted security workflow, could obtain some exception right,
  allowing them to upload their own compiled binaries. I am speaking of
  teams for instance like G'MIC, developed for many years by well known
  Free Software developers, within a public research institute and
  university (accountable and reliable).

Ideally we would have the infrastructure to allow people to upload their
code (or we would have control of their repository, see `Future future…`
section below) and our servers could just build their plug-ins for each
and every supported platform (Linux, Windows, macOS and other UNIXes).
But this is definitely not a point where we are currently and we should
not plan this at such early state.

### Future future…

Something I didn't want to add in early docs because I didn't want
people to get excited about it too soon, especially as we have been
speaking about it in meetings already years ago (and everyone was only
speaking about it, as though it was the main point. It's not): in a
possible future, we might want to provide source hosting for extension
creators. It could take the shape of a self-hosted Gitlab where
extension creators could upload their brushes, themes, plug-in code or
whatnot, provide CI and format checking, and so on. And one could
publish a new version of one's extension by just tagging it (hence
updating the repository and so on, taking description from the
metadata).

This is not a new idea, I remember Wordpress was already doing this like
at least 15 years ago (back then, with subversion, hosted by Wordpress
itself; and you could publish a new version of your Wordpress plug-in by
just tagging your subversion tree; I have no idea how this evolved as I
have not developed Wordpress plug-ins lately).

But this implies a whole new can of worm. See again the `Static Code`
section as to why we want to avoid huge maintenance burden for now
(maybe some day, if we have a lot of contributors who want to maintain a
custom Gitlab-or-other hosting, why not…).
